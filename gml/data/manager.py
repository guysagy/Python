# -*- coding: utf-8 -*-
"""
@author: Guy Sagy
"""

import pandas
import seaborn as sns
import matplotlib.pyplot as plt

class DataManager:
    '''
    DataManager class handles data management tasks.
    '''
    def __init__(self):
        self.data = None;
        return
    def loadCsv(self, filePath):
        self.data = pandas.read_csv(filePath, low_memory=False)
        return
    def getData(self, copy=True):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        return self.data.copy() if copy == True else self.data
    def setData(self, data, copy=True):
        self.data = (data.copy() if copy == True else data)
        return
    def toNumeric(self, columnsList):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        # Source : http://www.mzan.com/article/15891038-pandas-change-data-type-of-columns.shtml
        self.data[columnsList] = self.data[columnsList].apply(pandas.to_numeric, errors='coerce')
        return
    def dropNa(self):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        self.data.dropna(axis=[0,1], how='any', thresh=None, subset=None, inplace=True)
        return   
    def plot(self, explanatoryColName, responseColName):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        sns.boxplot(x=explanatoryColName, y=responseColName, data=self.data, palette="PRGn")
        sns.despine(offset=10, trim=True)
        plt.show()
        return
    def keepColumns(self, columnsList):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        self.data = self.data[columnsList]
        return
    def cut(self, columnName, intervalColumnName, binsList, labelsList, right=True):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        self.data[intervalColumnName]  = pandas.cut(self.data[columnName], bins=binsList, labels=labelsList, right=right) 
        return
    def printHead(self, n=10):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        print(self.data.head(n))
        return