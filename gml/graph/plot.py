# -*- coding: utf-8 -*-
"""
Created on Sat Jan 14 11:13:28 2017

@author: Guy
"""
import seaborn
import matplotlib.pyplot as plt
import gml.data.manager as gdm

class Plotter(gdm.DataManager):
    def __init(self):
        super.__init__();
        return;
    def showScatterPlot(self, xLabel, yLabel, xColumnName, yColumnName, printTitle=True):
        if printTitle == True:
            print("")
            print('Scatterplot for the Association Between {0} and {1}:'.format(xLabel, yLabel))
            print('================================================================================')
        seaborn.regplot(x=xColumnName, y=yColumnName, scatter=True, data=self.data)
        plt.xlabel(xLabel)
        plt.ylabel(yLabel)
        plt.title ('{0} Vs. {1}'.format(xLabel, yLabel))
        plt.show()
        return