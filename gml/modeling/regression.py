# -*- coding: utf-8 -*-
"""
@author: Guy Sagy
"""

import statsmodels.formula.api as smf
import gml.statistics.hypothesis_tests as gds

class Regression(gds.BasicInferentialAnalysis):
    def __init__(self):
        super().__init__()
        return
    def getOlsSimpleRegressionModel(self, explanatoryColName, responseColName, explanatoryLabel = "", responseLabel = "", printSummary = False):
        model = smf.ols('{1} ~ {0}'.format(explanatoryColName, responseColName), data = self.data).fit()
        if printSummary == True:
            print("")
            if explanatoryLabel == "":
                 explanatoryLabel = explanatoryColName
            if responseLabel == "":
                 responseLabel = responseColName
            print("OLS Regression Model for the association between {0} and {1}:".format(explanatoryLabel, responseLabel))
            print("======================================================================================================")
            print(model.summary()) 
        return model