# -*- coding: utf-8 -*-
"""
@author: Guy Sagy
"""

import pandas
import statsmodels.formula.api as smf
import statsmodels.stats.multicomp as multi   
import seaborn as sns
import matplotlib.pyplot as plt
import scipy.stats
import itertools
import gml.data.manager as gdt

class BasicInferentialAnalysis(gdt.DataManager):
    '''
    Base class for all classes that perform hypothesis tests.
    '''
    def __init__(self):
        self.data = None;
        return
    def getMean(self, explanatoryColName, printSummary = False, columnTitle = ""):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        mean = self.data[explanatoryColName].mean()
        if printSummary == True:
            if columnTitle == "":
                columnTitle = explanatoryColName
            print("")
            print("Mean value of {0}:".format(columnTitle))
            print("==============================================")
            print("{0:.2f}".format(mean))
        return mean
    def getStd(self, explanatoryColName, printSummary = False):
        if (self.data is None):
            raise Exception("Data  was not loaded!")
        std = self.data[explanatoryColName].std()
        if printSummary == True:
            print("")
            print("Standard deviation:")
            print("===================")
            print(std)
        return std
    def center(self, explanatoryColName):
        mean = self.getMean(explanatoryColName, printSummary = False)
        centerLambda = lambda v : v - mean
        self.data[explanatoryColName] = self.data[explanatoryColName].apply(centerLambda)
        return

class AnovaAnalyzer(BasicInferentialAnalysis):
    '''
    Use this AnovaAnalyzer class to perform an ANOVA analysis to carry out hypothesis testing when the explanatory variable is categorical and the response variable is quantitative.
    '''
    def __init__(self):
        super().__init__()
        return
    def doOls(self, explanatoryColName, responseColName):
        tu = (responseColName, explanatoryColName)
        formula = '{0} ~ C({1})'.format(*tu)
        self.olsModel = smf.ols(formula=formula, data=self.data).fit()
        return
    def outputOls(self):
        print(self.olsModel.summary())
        return
    def doMcl(self, explanatoryColName, responseColName):    
        self.mc1 = multi.MultiComparison(self.data[responseColName], self.data[explanatoryColName])
        return
    def outputMcl(self):
        mclTukeyHsd = self.mc1.tukeyhsd()
        print(mclTukeyHsd.summary())
        return

class ChiSquareAnalyzer(BasicInferentialAnalysis):
    '''
    Use this ChiSquareAnalyzer class to perform a Chai Square Test of Independence analysis to carry out hypothesis testing when the explanatory variable is categorical and the response variable is categorical too.
    '''
    def __init__(self):
        super().__init__()
        return
    def outputCrossTabs(self, explanatoryColName, responseColName):
        print("")
        print("Contingency table of observed counts:")
        print("=====================================")
        self.crossTabs = pandas.crosstab(self.data[responseColName], self.data[explanatoryColName])
        print(self.crossTabs)
        return
    def outputColumnPercentages(self):
        print("")
        print("Column Percentages:")
        print("===================")
        colsum = self.crossTabs.sum(axis=0)
        colpct = self.crossTabs/colsum
        print(colpct)
        return
    def outputChiSquareContingency(self):
        print("")
        print ("chi-square value, p value, expected counts")
        print("===========================================")
        self.ChiSquare = scipy.stats.chi2_contingency(self.crossTabs)
        print(self.ChiSquare)
        return
    def outputMultiComparisonChiSquareContingency(self, explanatoryCategoriesList, explanatoryColName, responseColName):
        for pair in itertools.combinations(explanatoryCategoriesList,2):
            ct = pandas.crosstab(self.data[explanatoryColName].isin(pair), self.data[responseColName])
            print("")
            print("chi sq test of subcategory: {}".format(pair))
            print("===========================================")
            print(scipy.stats.chi2_contingency(ct))
        return
        
class PearsonRAnalyzer(BasicInferentialAnalysis):
    '''
    Use this PearsonRAnalyzer class to perform a Pearson Correlation analysis to carry out hypothesis testing when the explanatory variable is quantitative and the response variable is quantitative too. 
    '''
    def __init__(self):
        super().__init__()
        return
    def outputPearsonRStatistics(self, explanatoryColName, responseColName):
        print ("(PearsonR correlation, p-value) ~= {0}".format(scipy.stats.pearsonr(self.data[explanatoryColName], self.data[responseColName])))
        return
    def outputScatterPlot(self, explanatoryColName, responseColName):
        sns.regplot(x=explanatoryColName, y=responseColName, fit_reg=True, data=self.data)
        plt.xlabel(explanatoryColName)
        plt.ylabel(responseColName)
        plt.show();
        return;